package Lesson7.Task1;

import java.util.concurrent.*;


public class Dock {

    private  final  int  COUNT = 10;

    Dock(){
            ExecutorService executor;
            executor = Executors.newFixedThreadPool(2);

            try {
                executor.execute(new Ship("Voevoda", COUNT));
                executor.execute(new Ship("Maersk", COUNT));
                executor.execute(new Ship("Tanner", COUNT));
            } finally {
                executor.shutdown();
            }
    }
}

