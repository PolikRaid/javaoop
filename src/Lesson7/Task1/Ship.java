package Lesson7.Task1;

public class Ship implements Runnable {

    private String shipName;
    private int boxNum;

    public Ship(String shipName, int boxNum) {
        this.shipName = shipName;
        this.boxNum = boxNum;
        new Thread(this);
    }

    public Ship() {
    }

    public String getShipName() {
        return shipName;
    }

    public void setShipName(String shipName) {
        this.shipName = shipName;
    }

    public synchronized void unload(int boxNum) {
        for (int i = 0; i < boxNum; i++) {
            try {
                System.out.println("Ship "+shipName+" unloaded box #"+(i+1));
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void run() {
        unload(boxNum);
    }
}
