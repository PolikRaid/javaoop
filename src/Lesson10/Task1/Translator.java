package Lesson10.Task1;

import java.io.*;
import java.util.Map;

public class Translator {

    Dictionary dictionary;

    public Translator() {

    }

    public Translator(Dictionary dictionary) {
        this.dictionary = dictionary;
    }

    public File Translate(File a, File c) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(a));
            BufferedWriter bw = new BufferedWriter(new FileWriter(c));

            Map map = dictionary.jsonToMap(dictionary.getDictFile());

            String line;
            while ((line = br.readLine()) != null) {
                String[] splitToWords = line.replaceAll("[^\\w\\s]","").split(" ");
                System.out.println("LineA - "+line);
                for(String word : splitToWords) {
                    System.out.println("word - "+word);
                    if(map.get(word)!=null){
                        line = line.replaceAll(word, map.get(word).toString());
                        System.out.println("LineB - "+line);
                    } else {
                        System.out.println("No translation for word "+word+" found");
                    }
                }
                bw.write(line+ System.lineSeparator());
            }
            br.close();
            bw.close();

        } catch ( IOException e) {
            e.printStackTrace();
        }
        return c;
    }

    public File TranslateWithMapDictionary(File a, Map map, File c) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(a));
            BufferedWriter bw = new BufferedWriter(new FileWriter(c));

            String lineA;
            while ((lineA = br.readLine()) != null) {
                String[] splitToWords = lineA.replaceAll("[^\\w\\s]","").split(" ");
                System.out.println("LineA - "+lineA);
                for(String word : splitToWords) {
                    System.out.println("word - "+word);
                    if(map.get(word)!=null){
                        lineA = lineA.replaceAll(word, map.get(word).toString());
                        System.out.println("LineB - "+lineA);
                    }
                }
                bw.write(lineA+ System.lineSeparator());
            }
            br.close();
            bw.close();

        } catch ( IOException e) {
            e.printStackTrace();
        }
        return c;
    }
}
