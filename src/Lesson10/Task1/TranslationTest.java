package Lesson10.Task1;

/*      1. Написать программу - переводчик, которая будет переводить текст в файле English.in, написанный на английском языке, на
        украинский язык, согласно заранее составленному словарю. Результат сохранить в файл Ukrainian.out.
        2. Сделать ф-ю ручного наполнения словаря и возможность его сохранения на диск.*/

import java.io.File;

public class TranslationTest {
    public static void main(String[] args) {

        File inputFile = new File("src/Lesson10/Task1/documents/english.in");
        File outputFile = new File("src/Lesson10/Task1/documents/ukrainian.out");

        File dictFile = new File("src/Lesson10/Task1/documents/dict.file");
        Dictionary dict1 = new Dictionary(dictFile);
        dict1.addTranslationPair("is", "е");
        Translator transUSUA = new Translator(dict1);

        dict1.addTranslationPairManually();

        transUSUA.Translate(inputFile, outputFile);
    }
}
