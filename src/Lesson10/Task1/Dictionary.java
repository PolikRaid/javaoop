package Lesson10.Task1;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import java.io.*;
import java.util.*;

public class Dictionary {

    private String word;
    private String translation;
    private Map<String, String> dictionary = new TreeMap<>();
    private File dictFile;

/*
    public Dictionary(Map<String, String> dictionary) {
        this.dictionary = dictionary;
    }
*/

    public Dictionary(File dictFile) {
        this.dictFile = dictFile;
    }

    public Dictionary() {
    }

    public Map<String, String> getDictionary() {
        return dictionary;
    }

    public void setDictionary(Map<String, String> dictionary) {
        this.dictionary = dictionary;
    }

    public File getDictFile() {
        return dictFile;
    }

    public void setDictFile(File dictFile) {
        this.dictFile = dictFile;
    }

    public void addTranslationPair(String word, String translation) {
        this.dictFile = getDictFile();
        Map<String, String> map = jsonToMap(dictFile);
        map.put(word,translation);
        mapToJson(map, dictFile);
    }

    public void addTranslationPairManually() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter word:");
        String word = sc.next();
        System.out.println("Please enter translation:");
        String translation = sc.next();

        this.dictFile = getDictFile();
        Map<String, String> map = jsonToMap(dictFile);
        map.put(word,translation);
        mapToJson(map, dictFile);
    }

    public void mapToJson(Map<String, String> dictionary, File file) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String gsonSt = gson.toJson(dictionary);
        try(PrintWriter pw = new PrintWriter(file)){
            pw.println(gsonSt);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public Map<String, String> jsonToMap(File file) {
        Gson gson = new Gson();
        Map<String, String> map = new HashMap<>();
        try {
            map = gson.fromJson(new FileReader(file), Map.class);
        } catch (JsonIOException | JsonSyntaxException | FileNotFoundException e) {
            e.printStackTrace();
        }
        return map;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dictionary that = (Dictionary) o;
        return word.equals(that.word) &&
                translation.equals(that.translation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(word, translation);
    }
}
