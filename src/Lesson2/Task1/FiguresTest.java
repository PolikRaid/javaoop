package Lesson2.Task1;

public class FiguresTest {

/*    1. Создайте абстрактный класс Shape, в котором есть два абстрактных метода double getPerimetr() и double getArea().
      2. Создайте класс Point, в котором есть два свойства double x double y.
      3. Создайте классы, которые описывают, как минимум, три геометрические фигуры (они должны быть подклассами
      Shape). При этом они в качестве свойств должны содержать классы Point.
      4. Создайте класс доска. Доска поделена на 4 части в каждую часть доски можно положить одну из фигур. У доски должны
      быть методы которые помещают и удаляют фигуру с доски. Также должен быть метод который выводит информацию о всех фигурах лежащих на доске и их суммарную площадь.*/

    public static void main(String[] args) {
        Point pointT1 = new Point(3, 5);
        Point pointT2 = new Point(5, 2);
        Point pointT3 = new Point(0, 0);
        Triangle tri1 = new Triangle(pointT1, pointT2, pointT3);
        Shape sh1 = tri1;
        System.out.println("Triangle perimeter is "+sh1.getPerimeter());
        System.out.println("Triangle area is "+sh1.getArea());
        System.out.println("");

        Circle cir1 = new Circle(pointT1, pointT2);
        System.out.println("Circle perimeter is "+cir1.getPerimeter());
        System.out.println("Circle area is "+cir1.getArea());
        System.out.println("");

        Square sqr1 = new Square(pointT3, pointT2);
        System.out.println("Square perimeter is "+sqr1.getPerimeter());
        System.out.println("Square area is "+sqr1.getArea());
        System.out.println("");

        Board board = new Board();
        board.setFigureOnBoard(1, sqr1);
        board.setFigureOnBoard(3, cir1);
        board.setFigureOnBoard(2, tri1);
        board.setFigureOnBoard(4, tri1);
        System.out.println(board.toString());
        System.out.println("Sum of figures area on board is "+board.getAreaFiguresOnBoard());
        for (int i=0; i<board.getFiguresOnBoard().length; i++){
            System.out.println(i+" "+board.getFiguresOnBoard()[i]+" ");
        }
        System.out.println();
        board.eraseFigureFromBoard(2);
        System.out.println("Sum of figures area on board AFTER ERASE "+board.getAreaFiguresOnBoard());

    }
}
