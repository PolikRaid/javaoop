package Lesson2.Task1;

public class Triangle extends Shape {

    private Point point1;
    private Point point2;
    private Point point3;

    public Triangle(Point point1, Point point2, Point point3) {
        this.point1 = point1;
        System.out.println(point1+"point1 "+this.point1+" this");
        this.point2 = point2;
        this.point3 = point3;
    }

    public Point getPoint1() {
        return this.point1;
    }

    public void setPoint1(Point point1) {
        this.point1 = point1;
    }

    public Point getPoint2() {
        return this.point2;
    }

    public void setPoint2(Point point2) {
        this.point2 = point2;
    }

    public Point getPoint3() {
        return this.point3;
    }

    public void setPoint3(Point point3) {
        this.point3 = point3;
    }

/*    Point gPoint1 = new Point(3,5);
    Point gPoint2 = new Point(6,2);
    Point gPoint3 = new Point(1,1);*/
/*    double a = getLenght(this.point1, this.point2);
    double b = getLenght(this.point2, this.point3);
    double c = getLenght(this.point3, this.point1);*/

/*
    double a = getLenght(getPoint1(), getPoint2());
    double b = getLenght(getPoint2(), getPoint3());
    double c = getLenght(getPoint3(), getPoint1());
*/


    /*double a = getLenght(gPoint1, gPoint2);
    double b = getLenght(gPoint2, gPoint3);
    double c = getLenght(gPoint3, gPoint1);

*/

    /*double a = getLenght(this.point1, this.point2);
    double b = getLenght(this.point2, this.point3);
    double c = getLenght(this.point3, this.point1);
*/


    private double getTriangleSidesLength (Point trianglePoint1, Point trianglePoint2){
        Point tPoint1 = trianglePoint1;
        Point tPoint2 = trianglePoint2;
        double length = getLenghts(tPoint1, tPoint2); //получает длины сторон треугольника
        return length;
    }

    @Override
    public double getPerimeter(){
        double a = getTriangleSidesLength(point1, point2);
        double b = getTriangleSidesLength(point2, point3);
        double c = getTriangleSidesLength(point3, point1);
        double perimeter = a + b + c;

        return perimeter;
    }

    @Override
    public double getArea() {
        double p; //полуперимето
        double s; //площадь
        double a = getTriangleSidesLength(point1, point2);
        double b = getTriangleSidesLength(point2, point3);
        double c = getTriangleSidesLength(point3, point1);

        p = (a + b + c) / 2;
        s = Math.sqrt(p * (p - a) * (p - b) * (p - c));

        return s;
    }

    @Override
    public String toString() {
        return "Triangle{" +
                "perimeter=" + getPerimeter() +
                ", area=" + getArea() +
                '}';
    }
}
