package Lesson2.Task1;

public abstract class Shape{

    public abstract double getPerimeter();

    public abstract double getArea();

    public double getLenghts(Point sidePoint1, Point sidePoint2){
        double length = Math.sqrt(Math.pow(sidePoint2.getX() - sidePoint1.getX(), 2)+Math.pow(sidePoint2.getY() - sidePoint1.getY(),2));
        return length;
    }

}
