package Lesson2.Task1;

public class Point {
    private double x;
    private double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Point() {
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

/*    public double getDist(Point p){
        double distance = Math.sqrt(Math.pow(sidePoint2.getX() - sidePoint1.getX(), 2)+Math.pow(sidePoint2.getY() - sidePoint1.getY(),2));;
        return distance;
                //TrianglePerimeter - a.getDist(b)+b.getDist(c)+c.getDist(c)
    }*/

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
