package Lesson2.Task1;

public class Circle extends Shape{

    private Point point1; //центральная точка
    private Point point2; //точка на окружности

    public Circle(Point point1, Point point2) {
        this.point1 = point1;
        this.point2 = point2;
    }

    public Circle() {
    }

    public Point getPoint1() {
        return point1;
    }

    public void setPoint1(Point point1) {
        this.point1 = point1;
    }

    public Point getPoint2() {
        return point2;
    }

    public void setPoint2(Point point2) {
        this.point2 = point2;
    }

    //Point gPoint1 = new Point(3,5);
    //Point gPoint2 = new Point(6,2);

    //double r = getLenghts(gPoint1, gPoint2);

    private double getCircleRadiusLength (){
        this.point1 = getPoint1();
        this.point2 = getPoint2();
        double radius = getLenghts(point1, point2); //получает длину радиуса круга
        return radius;
    }

    @Override
    public double getPerimeter(){
        double r = getCircleRadiusLength();
        double perimeter = 2*Math.PI*r;
        return perimeter;
    }

    @Override
    public double getArea() {
        double r = getCircleRadiusLength();
        double s = Math.PI*Math.pow(r,2);
        return s;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "perimeter=" + getPerimeter() +
                ", area=" + getArea() +
                '}';
    }
}
