package Lesson2.Task1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Board{

    private Shape[] boardFigures = new Shape[4];

    public Board() {
    }

    public void setFigureOnBoard(int sectorNum, Shape figure) {
        boardFigures[sectorNum-1] = figure;
    }

    public void eraseFigureFromBoard(int sectorNum) {
        boardFigures[sectorNum-1] = null;
    }

    public Shape[] getFiguresOnBoard(){
        return boardFigures;
    }


    public double getAreaFiguresOnBoard(){
        double figuresArea=0;
        for(Shape figure: boardFigures){
            if(figure != null) {
                figuresArea = figuresArea + figure.getArea();
            }
        }
        return figuresArea;
    }

    @Override
    public String toString() {
        return "Board{" +
                ", boardFiguresPoints=" + Arrays.toString(boardFigures) +
                ", boardFigures=" + Arrays.stream(boardFigures).map(Shape::toString).toArray(String[]::new) +
                '}';
    }
}
