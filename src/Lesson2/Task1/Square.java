package Lesson2.Task1;

public class Square extends Shape {

    //описывает квадрат по двум точкам его диагонали
    private Point point1;
    private Point point2;

    public Square(Point point1, Point point2) {
        this.point1 = point1;
        this.point2 = point2;
    }

    public Square() {
    }

    public Point getPoint1() {
        return point1;
    }

    public void setPoint1(Point point1) {
        this.point1 = point1;
    }

    public Point getPoint2() {
        return point2;
    }

    public void setPoint2(Point point2) {
        this.point2 = point2;
    }

    private double getSquareDiagonalLength (){
        this.point1 = getPoint1();
        this.point2 = getPoint2();
        double diagonal = getLenghts(point1, point2); //получает длину диагонали квадрата
        return diagonal;
    }

    @Override
    public double getPerimeter(){
        double diag = getSquareDiagonalLength();
        double perimeter = (4 * diag) / Math.sqrt(2); //вычисляем периметр квадрата из диагонали
        return perimeter;
    }

    @Override
    public double getArea() {
        double diag = getSquareDiagonalLength();
        double s = (diag * diag) / 2; //вычисляем площадь квадрата из диагонали
        return s;
    }

    @Override
    public String toString() {
        return "Square{" +
                "perimeter=" + getPerimeter() +
                ", area=" + getArea() +
                '}';
    }
}
