package Lesson9.Task3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CharStatistics {

    public CharStatistics() {
    }

    Map<Character, Integer> stat = new HashMap<>();

    public void getCharStats(File file) {
        char[] charList;

        try{
            BufferedReader br = new BufferedReader(new FileReader(file));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                String letters = line.replaceAll("[^\\w]","");
                sb.append(letters);
            }
            br.close();
            charList = sb.toString().toCharArray();
            System.out.println(charList);

            for (int i=0; i<charList.length; i++) {
                Integer n = stat.get(charList[i]);
                Character chari = charList[i];
                if(n == null){
                    stat.put(chari, 1);
                } else {
                    stat.put(chari, n+1);
                }
            }
            for (Character element:stat.keySet()) {
                System.out.println("char: "+element+" numberOfAppearance: "+stat.get(element));
            }
        } catch ( IOException e) {
            e.printStackTrace();
        }
    }
}
