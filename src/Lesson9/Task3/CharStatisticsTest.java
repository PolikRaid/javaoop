package Lesson9.Task3;

/*
3) Считайте из файла текст на английском языке, вычислите относительную частоту повторения каждой буквы и выведите на экран результат в порядке убывания относительной частоты повторения.
*/

import java.io.File;

public class CharStatisticsTest {
    public static void main(String[] args) {
        File inputFile = new File("src/Lesson10/Task1/documents/english.in");
        CharStatistics cstat = new CharStatistics();
        cstat.getCharStats(inputFile);
    }
}
