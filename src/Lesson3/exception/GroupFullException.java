package Lesson3.exception;

public class GroupFullException extends Exception {
    private String exceptionMessage = "Could not add student. Group is full";

    public GroupFullException() {
        this.exceptionMessage = exceptionMessage;
    }

    public String getExceptionMessage() {
        return exceptionMessage;
    }

}
