package Lesson3;

public class Student extends Human{
    private Double avgScore; //средний балл

    public Student(String firstName, String lastName, int age, boolean sex, Double avgScore) {
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setAge(age);
        this.setSex(sex);
        this.avgScore = avgScore;
    }

    public Student() {
    }

    public Double getAvgScore() {
        return avgScore;
    }

    public void setAvgScore(Double avgScore) {
        this.avgScore = avgScore;
    }

    @Override
    public String toString() {
        return "Student{" +
                "firstName='" + super.getFirstName() + '\'' +
                ", lastName='" + super.getLastName() + '\'' +
                ", age='" + super.getAge() + '\'' +
                ", sex(isMail)='" + super.getSex() + '\'' +
                ", avgScore=" + avgScore +
                '}';
    }
}
