package Lesson3;

import Lesson3.exception.GroupFullException;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class Group implements Voenkom{

    private Student group[]= new Student[10];

    public void addStudent(Student student){
        int studentsNum=0;
        try {

            for (int i = 0; i < group.length; i++) {
                if (group[i] == null) {
                    group[i] = student;
                    return;
                } else {
                    studentsNum++;
                    if (studentsNum == group.length) {
                        throw new GroupFullException();
                    }
                }
            }

            }catch (GroupFullException e) {
            System.out.println(e.getExceptionMessage());
            e.printStackTrace();
        }
    }


    public void addStudent(){
        Scanner sc = new Scanner(System.in);

        System.out.println("Please enter students first and last name, age, isSexMale and average score");

        String firstName = sc.next();
        String lastName = sc.next();
        int age = sc.nextInt();
        Boolean sex = sc.nextBoolean();
        Double avgScore = sc.nextDouble();

        Student newStudent = new Student(firstName, lastName, age, sex, avgScore);
    try {
        int studentsNum = 0;
        for (int i = 0; i < group.length; i++) {
            if(group[i]==null){
                    group[i] = newStudent;
                    break;
            } else {
                studentsNum++;
                if (studentsNum == group.length) {
                    throw new GroupFullException();
                }
            }
        }
    }catch (GroupFullException e) {
        System.out.println(e.getExceptionMessage());
        e.printStackTrace();
    }
    }

    public Boolean deleteStudent(String firstName, String lastName){
        for (int i = 0; i <group.length ; i++) {

            if ((group[i]!=null) && group[i].getFirstName().equalsIgnoreCase(firstName) && group[i].getLastName().equalsIgnoreCase(lastName)) {
                group[i] = null;
                return true;
            }
        }
        //Following FOR cycle return true but do not deleted student from a group
            /*for (Student student : group) {
                if ((student!=null) && student.getFirstName().equalsIgnoreCase(firstName) && student.getLastName().equalsIgnoreCase(lastName)) {
                    student = null;
                    return true;
                }
            }*/
            return false;
    }

    public Student[] sortStudentByLastName(){
        Arrays.sort(group, new StudentLastNameComparator());
        return group;
    }

    public Student[] sortStudentByParam(String sortByParam){
        if (sortByParam=="LastName") {
            Arrays.sort(group, new StudentLastNameComparator());
        } else if (sortByParam=="Age") {
            Arrays.sort(group, new StudentAgeComparator());
        } else if (sortByParam=="AvgScore") {
            Arrays.sort(group, new StudentAvgScoreComparator());
        }
        return group;
    }

    @Override
    public Student[] getRecrut() {
        int recrutNum=0;
        for (Student student : group) {
            if((student!=null) && (student.getSex()==true) && (student.getAge()>18)){
                recrutNum++;
            }
        }
        Student[] recrutList = new Student[recrutNum];
        int k = 0;
        for (Student student : group) {
            if((student!=null) && (student.getSex()==true) && (student.getAge()>18)){
                recrutList[k] = student;
                k++;
           }
        }
        return recrutList;
    }

    @Override
    public String toString() {
        //this.group = sortStudentByLastName();
        String groupList = "GroupOfStudents{" + "group=";
        for (int i = 0; i <this.group.length ; i++) {
            if(group[i]!=null) {
                groupList = groupList + "\n" + group[i];
            }
        }
        return groupList;
    }
}

