package Lesson3;

/*      1) Создайте класс, описывающий человека (создайте метод, выводящий информацию о человеке).
        2) На его основе создайте класс студент (переопределите метод вывода информации).
        3) Создайте класс Группа, который содержит массив из 10 объектов класса Студент. Реализуйте методы добавления, удаления студента и метод поиска студента по фамилии. В
        случае попытки добавления 11 студента, создайте собственное исключение и обработайте его.
        Определите метод toString() для группы так, что бы он выводил список студентов в алфавитном порядке.

        1. Усовершенствуйте класс Group, добавив возможность интерактивного добавления объекта.
        2. Реализуйте возможность сортировки списка студентов по фамилии.
        3. Реализуйте возможность сортировки по параметру(Фамилия, успеваемость и т. д.).
        4. Реализуйте интерфейс Военком, который вернет из группы массив студентов - юношей, которым больше 18 лет.
*/


public class StudentTest {
    public static void main(String[] args) {
    Group group = new Group();
    Student student1 = new Student("Oleg", "Belyi", 33, true, 3.44);
    Student student2 = new Student("Masha", "Leyka", 23, false, 4.42);
    Student student3 = new Student("Ivan", "Anosov", 22, true, 4.04);
    Student student4 = new Student("Kolya", "Dobrov", 17, true, 4.42);
    Student student5 = new Student("Ivanna", "Orlova", 19, false, 4.72);
    Student student6 = new Student("Teo", "Orlov", 19, true, 5.00);
    Student student7 = new Student("Fedor", "Korol", 29, true, 4.30);
    Student student8 = new Student("Petro", "Kozlov", 18, true, 4.90);
    Student student9 = new Student("Ivana", "Gorov", 19, false, 4.55);
    Student student10 = new Student("Hlenn", "Oikova", 19, false, 4.55);
    Student student11 = new Student("Gorch", "KOKO", 19, true, 4.55);
    group.addStudent(student1);
    group.addStudent(student2);
    group.addStudent(student3);
    group.addStudent(student4);
    group.addStudent(student5);
    group.addStudent(student6);
    group.addStudent(student7);
    group.addStudent(student8);
    group.addStudent(student9);
    group.addStudent(student10);
    group.addStudent(student11);
    group.addStudent(); //add student manually
    System.out.println(group.toString());
    group.sortStudentByLastName();

    System.out.println(group.deleteStudent("Teo", "Orlov")); //delete student

         System.out.println("List of students after deletion");
    System.out.println(group.toString());

    group.sortStudentByParam("Age");
    System.out.println("List of students after sorting by Age:");
    System.out.println(group.toString());

    group.sortStudentByParam("AvgScore");
    System.out.println("List of students after sorting by AvgScore:");
    System.out.println(group.toString());

    Student recruts[] = group.getRecrut();

        System.out.println("List of recruts:");
        for (int i = 0; i < recruts.length; i++) {
            System.out.println((i+1)+") "+recruts[i]);
        }
    }
}
