package Lesson3;

import java.util.Comparator;

public class StudentLastNameComparator implements Comparator {

    @Override
    public int compare(Object o1, Object o2) {
        if (o1!=null&&o2==null){
            return -1;
        }
        if (o1==null&&o2!=null){
            return 1;
        }
        if (o1==null&&o2==null){
            return 0;
        }
        Student student1 = (Student) o1;
        Student student2 = (Student) o2;

        return student1.getLastName().compareTo(student2.getLastName());
    }
}
