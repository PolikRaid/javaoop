package Lesson1.Task1;

public class Cat extends Animals{
    private String name;
    private String type;

    public Cat(int age, double weigt, boolean sex, String ration, String name, String type) {
        super(age, weigt, sex, ration); //вызов конструктора Анималс с 4 параметрами
        this.name = name;
        this.type = type;
    }

    public Cat(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public Cat() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Cat{" +
                "name='" + name + '\'' +
                ", type='" + type + '\'' +
                super.toString() + '\'' +
                '}';
    }

    @Override
    public void getVoice(){ //override method - hide Alimals implementation
        System.out.println("Murrr");
    }
}


