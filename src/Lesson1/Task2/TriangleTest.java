package Lesson1.Task2;

public class TriangleTest {
    public static void main(String[] args) {
        Triangle triangle1 = new Triangle(4, 4, 5);
        System.out.println("Triangle area is " + triangle1.getArea());
        triangle1.setB(-14);
        System.out.println("Triangle area is " + triangle1.getArea()); //one side length has negative value
        triangle1.setB(55);
        System.out.println("Triangle area is " + triangle1.getArea()); //triangle does not exist
    }
}
