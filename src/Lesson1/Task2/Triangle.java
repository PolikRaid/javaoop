package Lesson1.Task2;

/*
Описать класс «Triangle». В качестве свойств возьмите длины сторон
        треугольника. Реализуйте метод, который будет возвращать площадь этого
        треугольника. Создайте несколько объектов этого класса и протестируйте их.
*/

public class Triangle {
    private double a;
    private double b;
    private double c;
    private double p;
    private double area;

    public Triangle(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public Triangle() {
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public double getC() {
        return c;
    }

    public void setA(double a) {
        this.a = a;
    }

    public void setB(double b) {
        this.b = b;
    }

    public void setC(double c) {
        this.c = c;
    }

    public double getArea() {
        if ((a > 0) && (b > 0) && (c > 0)) {
            if ((a+b)>c && (a+c)>b && (b+c)>a) {
                p = (a + b + c) / 2;
                System.out.println("p is " + p);
                area = Math.sqrt(p * (p - a) * (p - b) * (p - c));
                return area;
            }
            else {
                System.out.println("Such triangle does not exist");
                return 0;
            }
        }
        else {
            System.out.println("All triangle sides should have length value more that 0 ");
            return 0;
        }
    }
}
