package Lesson1.Task3;

import static java.lang.String.valueOf;

public class Vector3dTest {
    public static void main(String[] args) {
        Vector3d vectorTest1 = new Vector3d(3, -1, -2);
        Vector3d vectorTest2 = new Vector3d(1, 2, -1);
        System.out.println(vectorTest1);
        System.out.println(vectorTest2);

        Vector3d vectorTestSum = new Vector3d().sumVector(vectorTest1, vectorTest2);
        System.out.println("Sum of two vectors equals: " + vectorTestSum.toString());
        System.out.println("x: "+vectorTestSum.getX()+"y: "+vectorTestSum.getY()+"z: "+vectorTestSum.getZ());

        Vector3d vectorScalarMultiply = new Vector3d().scalarMultiplyVectors(vectorTest1, vectorTest2);
        System.out.println("Vectors scalar multiplication equals:" + vectorScalarMultiply.toStringScalarVector());

        Vector3d vectorVectorMultiply = new Vector3d().vectorMultiplyVectors(vectorTest1, vectorTest2);
        System.out.println("Vectors vector multiplication equals:" + vectorVectorMultiply.toString());
    }
}
