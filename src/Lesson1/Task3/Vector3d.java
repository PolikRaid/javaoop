package Lesson1.Task3;

import java.util.Vector;

public class Vector3d {
    private int x;
    private int y;
    private int z;
    private int a;

    public Vector3d(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector3d(int a) {
        this.a = a;
    }

    public Vector3d() {
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }

    public int getA() {
        return a;
    }


    public Vector3d sumVector (Vector3d vector1, Vector3d vector2){
        Vector3d resultVector = new Vector3d();
        resultVector.x = vector1.x + vector2.x;
        resultVector.y = vector1.y + vector2.y;
        resultVector.z = vector1.z + vector2.z;
        return resultVector;
    }

    public Vector3d scalarMultiplyVectors (Vector3d vector1, Vector3d vector2){
        Vector3d resultVector = new Vector3d(0);
        resultVector.a = (vector1.x * vector2.x) + (vector1.y * vector2.y) + (vector1.z * vector2.z);
        return resultVector;
    }


    public Vector3d vectorMultiplyVectors (Vector3d vector1, Vector3d vector2){
        Vector3d resultVector = new Vector3d();

        int i = (vector1.y * vector2.z) - (vector2.y * vector1.z);
        int j = (vector1.x * vector2.z) - (vector2.x * vector1.z);
        int k = (vector1.x * vector2.y) - (vector2.x * vector1.y);
        resultVector.x = i;
        resultVector.y = -j;
        resultVector.z = k;
        return resultVector;
    }

    @Override
    public String toString() {
        return "Vector3d{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }

    public String toStringScalarVector() {
        return "Vector3d{" +
                "a=" + a +
                '}';
    }
}
