package Lesson5;

/*
        1. Напишите программу, которая скопирует файлы с заранее определенным расширением(например, только doc) из каталога источника в каталог приемник.
        2. Напишите программу, которая примет на вход два текстовых файла, а вернет один. Содержимым этого файла должны быть слова, которые одновременно есть и в первом и во втором файле.
        3. Усовершенствуйте класс, описывающий группу студентов, добавив возможность сохранения группы в файл.
        4. Реализовать обратный процесс. Т.е. считать данные о группе из файла, и на их основе создать объект типа группа.
*/

import java.io.*;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
//import org.apache.commons.io.FileUtils;

public class FileManager {

    public boolean copyFile(File source, File recipient, String extension) {
        String[] acc = {extension};
        ExtFileFilter eFF = new ExtFileFilter(acc);
        if(source.isDirectory() && recipient.isDirectory()) {
            File[] fileList = source.listFiles(eFF);

            if(fileList.length == 0){
                return false;
            }
            try {
                for (File file : fileList) {
                    FileChannel sourceChannel = null;
                    FileChannel destination = null;
                    sourceChannel = new FileInputStream(file).getChannel();
                    destination = new FileOutputStream(recipient.getPath() + "/" + file.getName()).getChannel();
                    if (destination != null && source != null) {
                        destination.transferFrom(sourceChannel, 0, sourceChannel.size());
                    }
                }
                return true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public File joinFiles(File a, File b, File c) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(a));
            List<String> wordsInFile = new ArrayList<>();
            String lineA;
            while ((lineA = br.readLine()) != null) {
                String[] splitToWords = lineA.split(" ");
                for(String word : splitToWords) {
                    wordsInFile.add(word);
                }
            }
            br.close();

            StringBuilder sb = new StringBuilder();

            for (String wordB : wordsInFile) {
                Scanner scanner = new Scanner(b);
                int lineNum = 0;
                while (scanner.hasNextLine()) {
                    String line = scanner.nextLine();
                    lineNum++;
                    if(line.indexOf(wordB)>=0) {
                        sb.append(line.substring(line.indexOf(wordB),(line.indexOf(wordB)+wordB.length()))+", ");
                        break;
                    }
                }
            }
            BufferedWriter bw = new BufferedWriter(new FileWriter(c));
            bw.write(String.valueOf(sb));
            bw.close();

        } catch ( IOException e) {
            e.printStackTrace();
        }
        return c;
    }

    public void fileToString (File file) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                line = line.replace("\n"," ");
                sb.append(line);
            }
            br.close();

            System.out.println(sb);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
