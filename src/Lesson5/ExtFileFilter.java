package Lesson5;

import java.io.File;
import java.io.FileFilter;

public class ExtFileFilter implements FileFilter {
    private String[] acceptable;

    public ExtFileFilter(String[] acceptable) {
        this.acceptable = acceptable;
    }

    private boolean check(String ext){
        for(String stringExt : acceptable) {
            if (ext.equals(stringExt)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean accept(File pathname) {
        int pointerIndex = pathname.getName().lastIndexOf(".");
        if(pointerIndex==-1) {
            return false;
        }
        String ext = pathname.getName().substring(pointerIndex+1);
        return check(ext);
    }
}
