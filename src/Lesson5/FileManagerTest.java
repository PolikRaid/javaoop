package Lesson5;

/*
        1. Напишите программу, которая скопирует файлы с заранее определенным расширением(например, только doc) из каталога источника в каталог приемник.
        2. Напишите программу, которая примет на вход два текстовых файла, а вернет один. Содержимым этого файла должны быть слова, которые одновременно есть и в первом и во втором файле.
        3. Усовершенствуйте класс, описывающий группу студентов, добавив возможность сохранения группы в файл.
        4. Реализовать обратный процесс. Т.е. считать данные о группе из файла, и на их основе создать объект типа группа.
*/

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class FileManagerTest {

    public static void main(String[] args) {
        FileManager fm = new FileManager();
        File sourceFolder = new File("src\\Lesson5\\sourceFolder");
        File recipientFolder = new File("src/Lesson5/recipientFolder");
        System.out.println("IsDirectory: "+sourceFolder.isDirectory());

        System.out.println(fm.copyFile(sourceFolder, recipientFolder, "doc")); //copy doc files

        //Task: Show only words contains in both files
        File a = new File(sourceFolder+"/doc.docc.txt");
        File b = new File(sourceFolder+"/Test3.doc.txt");
        File c = new File(sourceFolder+"/JoinedFile.txt");
        System.out.print("File A: ");
        fm.fileToString(a);
        System.out.print("File B: ");
        fm.fileToString(b);
        System.out.print("File C: ");
        fm.fileToString(fm.joinFiles(a, b, c));
    }
}
